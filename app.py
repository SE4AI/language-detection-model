import streamlit as st
import time
import tensorflow as tf
import re

from db_functions import *
from predict import *

# Load model via path into the cache (allowing mutliple proccessor cores)
@st.cache(allow_output_mutation=True)
def load_model(path: str) -> str:
    if path == '':
        return None
    else:
        return tf.keras.models.load_model(path)


# Detect language (one of three) for given text input using the provided lang model
def _detect_language(model, text: str):

    if (text.isdigit()):
        st.markdown(f'<h1 style="color:#FF0000;font-size:24px;">{"Error(digit as input): Not a valid input format."}</h1>', unsafe_allow_html=True)
        return "Error(digit as input)"

    lang_vector = {"en": "English",
                   "de": "German",
                   "es": "Spanish"}
                   
    language = "Gibberish"
    (lang, prob) = predict_lang(model, text)
    # if probability of prediction is lower than 40% assume false prediction (Gibberish)
    if (prob > 0.40):
        language = lang_vector[lang[0]]
    st.write("### This text appears to be the language: {}.".format(language))
    st.write("###### (with a probability of: {})".format(prob))
    st.write("## ##")


if __name__ == "__main__":

    if 'show_feedback' not in st.session_state:
        st.session_state.show_feedback = False

    language_model = load_model(
        path="training_pipeline/saved_model/simple_mlp_novectorize.h5")
    with st.sidebar:
        """
        This is a simple streamlit application that guesses the language of the text passed.
        """

    # Get user input for text that is to be predicted
    text_input = st.text_area(
        "Please enter sample text to detect language: ",
        value="",
        placeholder=None,
        disabled=False, 
    )

    button = st.button(
        "Detect language",
        on_click=_detect_language,
        args=(
            language_model,
            text_input,
        ),
    )

    # Use streamlit session state to keep the feedback selectbox visible (otherwise streamlit forces a reload)
    if button:
        st.session_state.show_feedback = True

    if st.session_state.show_feedback:

        st.markdown("## ##")
        st.write("Prediction not correct?")

        # Get user feedback on correct language for given text, transform into correct shorthand labels
        choice = st.selectbox("Choose correct language", ('de', 'en', 'es'),
                              format_func=lambda x: "German" if x == 'de' else ("English" if x == 'en' else "Spanish"))

        if st.button("Submit feedback"):
            # submit feedback to postgres db
            add_feedback(text_input, choice)
            # allow the feedback area to disapear (via session state), display the success state for 2 seconds, then reload page
            st.success("Thank you! Your feedback was submitted")
            st.session_state.show_feedback = False
            time.sleep(2)
            st.experimental_rerun()
