- GIT:
    - git status                    # status infos
	- git pull --all                # pull all branches
	- git branch -a                 # list all branches
    - git branch -c pytest_dev      # creates new branch
	- git checkout -b pytest_dev    # switch to branch
	- git branch                    # show local branch
	- git add . && git commit -m "" # commit local branch
	- git pull origin pytest_dev    # update branch
	- git push origin HEAD          # push branch and this creates sub-repo with the branchname
    - git push origin pytest_dev    # HEAD points after checkout to pytest_dev, so this cmd is also valid

    - git-repo-structure:
        - remotes/origin/HEAD -> local pointer auf die Branch mit der man gerade arbeitet, ändert sich mit git checkout
        - remotes/origin/main -> main-branch
        - remotes/origin/pytest_dev -> andere branches
    
    - Merge-Requests and Pipelines:
        - wenn man eine neue branch mit der main branch mergen möchte, kann man konfigurieren, dass, wenn der admin den request akzeptiert, z.B. nur die stage "test" ausgeführt wird
        - https://docs.gitlab.com/ee/ci/pipelines/index.html
        - https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html
        - im yml-file Pipeline nach Bedingungen (rules) ausführen:
            rules:
                - if: $CI_COMMIT_BRANCH == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"
        
        - Pipeline nur dann ausführen, wenn sich etwas ändert:
            rules:
                - changes:
                    - path/to/files

    - Freeze deployment: abhängig von einer Zeit-Variable, die man in den Settings setzen kann.
        - https://docs.gitlab.com/ee/user/project/releases/index.html#prevent-unintentional-releases-by-setting-a-deploy-freeze
        - im yml-file einfach in den Job setzen der nicht ausgeführt werden soll:
            rules:
                - if: $CI_DEPLOY_FREEZE == null
    
    - 