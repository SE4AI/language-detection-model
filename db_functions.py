import os
import psycopg2


# add the user prediction feedback to the postgres db
def add_feedback(input, language):

    try:
        # use the attached heroku db (https://db4langmodel.herokuapp.com/)
        DATABASE_URL = os.environ.get('HEROKU_POSTGRESQL_GOLD_URL')
    except KeyError:
        DATABASE_URL = ""

    if DATABASE_URL != "":
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cur = conn.cursor()

        cur.execute(
            "INSERT INTO feedback (label, text) VALUES (%s, %s)", (language, input))

        conn.commit()

        conn.close()
        cur.close()
    else:
        print("Couldn't fetch DB-URL!!!")


def view_all():

    try:
        DATABASE_URL = os.environ['DATABASE_URL']
    except KeyError:
        DATABASE_URL = ""

    if DATABASE_URL != "":
        conn = psycopg2.connect(DATABASE_URL, sslmode='require')
        cur = conn.cursor()

        cur.execute('SELECT * FROM feedback')
        data = cur.fetchall()

        conn.commit()
        conn.close()
        cur.close()
    else:
        print("Couldn't fetch DB-URL!!!")

    return data
