import pytest
from app import load_model, _detect_language, predict_lang

# Pytests for app.py
# Run locally with code coverage: 
# python -m pytest -vvv --cov="." --cov-config=".coveragerc" test_app.py

@pytest.fixture(scope='module')
def test_model():
    model = load_model(path="training_pipeline/saved_model/simple_mlp_novectorize.h5")
    return model

def test_load_model_with_empty_path():
    '''
    GIVEN tf-model
    WHEN empty path
    THEN return NONE
    '''
    assert load_model('') == None

def test__detect_language_with_text_input(test_model):
    '''
    GIVEN tf-model
    WHEN string input
    THEN assert output is one of the languages with a probability between 0 and 1
    '''

    mock_input = "Any Text"

    (lang, prob) = predict_lang(test_model, mock_input)

    assert lang[0] == "en" or "de" or "es" and 0 >= prob[0] <= 1 

def test__detect_language_with_digit_input(test_model):
    '''
    GIVEN tf-model
    WHEN string input
    THEN assert output is "Error(digit as input)"
    '''
    
    mock_input = "123456789"

    assert _detect_language(test_model, mock_input) == "Error(digit as input)"