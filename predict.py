import tensorflow as tf
import numpy as np
import pickle
from sklearn import preprocessing
from tensorflow.keras import layers

# Use the provided lang model and input text to predict the language of the text


def predict_lang(model, text: str):
    lang_list = ["es", "en", "de"]

    # init label encoder to load pickled vectorization
    le = preprocessing.LabelEncoder()
    le.fit(lang_list)

    # load pickled text vectorization
    from_disk = pickle.load(
        open("training_pipeline/saved_model/layer.pkl", "rb"))
    new_v = layers.TextVectorization.from_config(from_disk['config'])
    new_v.adapt(tf.data.Dataset.from_tensor_slices(["xyz"]))
    new_v.set_weights(from_disk['weights'])

    # apply vectorization apply to the given text (only that can be interpreted by the model)
    examples_vectorized = new_v([text])

    # predict and softmax the output as return values
    logits = model.predict(examples_vectorized)
    probits = tf.nn.softmax(logits)
    idx_predictions = np.argmax(probits, axis=1)

    return(le.inverse_transform(idx_predictions), np.max(probits, axis=1))
