# SE4AI Group 5: language-detection-model 

## Run web app locally
```bash
pip install -r requirements.txt

# if streamlit is on $PATH
streamlit run app.py  

# else
python -m streamlit run app.py
```

## Create new scraping and model data

The training of the model is achived through a dockerized python install.
Go into the training pipeline folder:

```bash
cd training_pipeline
```

Then execute the Dockerfile as specified in the README.

Optionally train the model by using:
```bash
### if you also want to scrape for new content
python train.py -s

### if you want to just train and save the model
python train.py
´´´