Build the docker image (using the name lang-model-train):
```bash
docker build --tag lang-model-train .
```

Run the docker image in the ```bash /training_pipeline ``` folder:
```bash
docker run -d \
  -it \
  --name lang-model-train \
  -v "$(pwd)":/app \
  lang-model-train:latest
```