import argparse
import tensorflow as tf
import pandas as pd
import pickle
import os
from sklearn import preprocessing

from tensorflow.keras import layers
from tensorflow.keras import losses

from scrape import scrapeList, get_feedback

DATA_DIRECTORY = "data"

# Use argprase to give user option to train without (time consuming) scraping part
parser = argparse.ArgumentParser()
parser.add_argument("-s", action='store_true',
                    help="Train without scraping first")
args = parser.parse_args()

if args.s:
    # Scrape for new data (using wikipedia)
    print("Scraping the web for text input data...")
    scrapeList(DATA_DIRECTORY)

# Load data in pandas for filtering
train_df = pd.read_csv(f"{DATA_DIRECTORY}/scrape_train.csv")
val_df = pd.read_csv(f"{DATA_DIRECTORY}/scrape_val.csv")
test_df = pd.read_csv(f"{DATA_DIRECTORY}/scrape_test.csv")

# Add feedback data to training dataframe
feedback = pd.DataFrame(get_feedback())
feedback = feedback.rename({0: 'labels', 1: 'text'}, axis=1)
train_df = pd.concat([train_df, feedback])

# Remove all empty rows
nan_value = float("NaN")
train_df.replace("", nan_value, inplace=True)
val_df.replace("", nan_value, inplace=True)
test_df.replace("", nan_value, inplace=True)

train_df.dropna(how='all', axis=1, inplace=True)
val_df.dropna(how='all', axis=1, inplace=True)
test_df.dropna(how='all', axis=1, inplace=True)

lang_list = ["es", "en", "de"]

# Create dictionary for enconding labels
le = preprocessing.LabelEncoder()
le.fit(lang_list)

num_classes = len(le.classes_)

train_labels = tf.keras.utils.to_categorical(
    le.transform(train_df.pop('labels')), num_classes=num_classes)
val_labels = tf.keras.utils.to_categorical(
    le.transform(val_df.pop('labels')), num_classes=num_classes)
test_labels = tf.keras.utils.to_categorical(
    le.transform(test_df.pop('labels')), num_classes=num_classes)

# Build the datasets using tf.data
raw_train_ds = tf.data.Dataset.from_tensor_slices(
    (train_df["text"].to_list(), train_labels))  # X, y
raw_val_ds = tf.data.Dataset.from_tensor_slices(
    (val_df["text"].to_list(), val_labels))
raw_test_ds = tf.data.Dataset.from_tensor_slices(
    (test_df["text"].to_list(), test_labels))

batch_size = 32
max_features = 12000  # top 12K most frequent words
sequence_length = 40  # Defined in the previous data exploration section

# Prepare dataset for training
vectorize_layer = layers.TextVectorization(
    standardize="lower_and_strip_punctuation",
    max_tokens=max_features,
    output_mode='int',
    output_sequence_length=sequence_length)

# Vectorize layer is fitted to the training data
vectorize_layer.adapt(train_df["text"].to_list())

# Create the final datasets using the vectorize_layer
# returns vectorize_layer(text), label
train_ds = raw_train_ds.map(lambda x, y: (vectorize_layer(x), y))
val_ds = raw_val_ds.map(lambda x, y: (vectorize_layer(x), y))
test_ds = raw_test_ds.map(lambda x, y: (vectorize_layer(x), y))

# Applying cache techniques for improving inference and training time
# It allows tensorflow to prepare the data while it trains the model
AUTOTUNE = tf.data.AUTOTUNE

train_ds = train_ds.batch(batch_size=batch_size)
train_ds = train_ds.prefetch(AUTOTUNE)
val_ds = val_ds.batch(batch_size=batch_size)
val_ds = val_ds.prefetch(AUTOTUNE)
test_ds = test_ds.batch(batch_size=batch_size)
test_ds = test_ds.prefetch(AUTOTUNE)

# Model training
embedding_dim = 16
model = tf.keras.Sequential([
    layers.Embedding(max_features + 1, embedding_dim),
    layers.Dropout(0.2),
    layers.GlobalAveragePooling1D(),
    layers.Dropout(0.2),
    layers.Dense(3)])

# Specify loss, optimizer and metrics
model.compile(loss=losses.CategoricalCrossentropy(from_logits=True, label_smoothing=0.1),
              optimizer='adam',
              metrics=['accuracy'])

# Train
epochs = 10
history = model.fit(
    train_ds,
    validation_data=val_ds,
    epochs=epochs)

#loss, accuracy = model.evaluate(test_ds)

if not os.path.exists('saved_model'):
    os.makedirs('saved_model')

# Save the model
print("Saving model and config data in folder: saved_model/")
model.save('saved_model/simple_mlp_novectorize.h5')

# Save the config/vector layers as pickle
pickle.dump({'config': vectorize_layer.get_config(),
             'weights': vectorize_layer.get_weights()}, open("saved_model/layer.pkl", "wb"))
