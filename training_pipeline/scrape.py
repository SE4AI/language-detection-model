import requests
import pandas as pd
from bs4 import BeautifulSoup
import re
import csv
import os
from tqdm import tqdm


# get current feedback stored in the seperated heroku postgres db as json object
def get_feedback():
    S = requests.Session()
    R = S.get(url='https://db4langmodel.herokuapp.com/getfeedback/')
    feedback = R.json()

    return feedback


# create url list of wiki article links using the country articles of prediction languages as seeds
def createUrlList():
    if os.path.exists("wiki_urls.txt"):
        os.remove("wiki_urls.txt")

    S = requests.Session()

    articles = {"en": "United_States",
                "de": "Deutschland",
                "es": "España"}

    for lang in articles:
        api = "https://"+lang+".wikipedia.org/w/api.php"

        PARAMS = {
            "generator": "links",
            "action": "query",
            "format": "json",
            "titles": articles[lang],
            "prop": "info",
            "inprop": "url",
            # split amount of articles saved based on empirical distribution
            "gpllimit": 320 if lang == 'es' else (180 if lang == 'en' else 170)
        }

        # request article metadata (json) from wikipedia api
        R = S.get(url=api, params=PARAMS)
        data = R.json()

        pages = data["query"]["pages"]
        f = open("wiki_urls.txt", "a")

        # save the canonical url for each article
        for k, v in tqdm(pages.items(), desc="Saving wiki urls ("+lang+")"):
            f.write(v["canonicalurl"]+"\n")

        f.close()


# shuffle the data and split the file into training, test and validation data (80 | 10 | 10)
def splitFile():
    df = pd.read_csv('scrape.csv')

    df = df.sample(frac=1)

    train = df.sample(frac=0.8)
    rest = df.loc[~df.index.isin(train.index)]

    test = rest.sample(frac=0.5)
    val = rest.loc[~rest.index.isin(test.index)]

    train.to_csv('data/scrape_train.csv', index=False)
    test.to_csv('data/scrape_test.csv', index=False)
    val.to_csv('data/scrape_val.csv', index=False)


# scrape the url list provided as param and store the content as sentences with label in csv
def scrapeList(path):

    if os.path.exists("scrape.csv"):
        os.remove("scrape.csv")

    if not os.path.exists(path):
        os.makedirs(path)

    createUrlList()

    # write csv header
    csv.writer(open('scrape.csv', 'w')).writerow(['labels', 'text'])

    urls = open('wiki_urls.txt', 'r')
    for url in tqdm(urls, desc="Saving text to csv", total=len(open('wiki_urls.txt').readlines())):

        # get language of article using the subdomain
        address = url.replace('\n', '')
        host = address.partition('://')[2]
        lang = host.partition('.')[0]

        response = requests.get(address)

        # get page content using BeautifulSoup lxml parser
        soup = BeautifulSoup(response.content, 'lxml')

        text = ''
        # break the text into paragraphs via html <p> tag
        for paragraph in soup.find_all('p'):
            text += paragraph.text

        # substitute quotations and citation of wiki texts
        text = re.sub(r'\[.*?\]+', '', text)
        text = text.replace('\n', ' ')
        text = re.sub("[\(\[].*?[\)\]]", "", text)

        # split (loosely) into senteces using regex punctions
        sentences = re.split(
            r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', text)

        file = csv.writer(open('scrape.csv', 'a'))
        for s in sentences:
            # substitue special characters expect country specific
            s = re.sub('[^a-zA-Z0-9äöüÄÖÜßñáéíóú \n\.\?\!]', '', s)
            # set minium and maximum sentence length
            if len(s.split()) > 250:
                file.writerow([lang, ' '.join(s.split()[: 250])])
            elif len(s.split()) > 2:
                file.writerow([lang, s])

    splitFile()
